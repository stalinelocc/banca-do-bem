<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header banner-home">
    <div class="container">
    <div class="row">
        <div class="col-md-3"></div>
            <div class="col-md-6">
        <h1>Compre apoiando grandes causas.</h1>
        <p>Só com a Banca do Bem você compra produtos incríveis e ainda gera impacto na renda de grandes causas.</p>  
                <a href="#" class="bt-white">Conheça a causa</a>
        </div>
        <div class="col-md-3"></div>
    </div>
    </div>
    </header>
    <section class="destaques-home">
    <div class="container">
    <div class="row">
      <div class="col-md-3">
        SIDEBAR
      </div>
      <div class="col-md-9">
        <h2>Conheça os nossos queridinhos</h2>
        
              <ul class="products">
          <?php
          $args = array(
            'post_type' => 'product',
            'posts_per_page' => 9
            );
          $loop = new WP_Query( $args );
          if ( $loop->have_posts() ) {
            while ( $loop->have_posts() ) : $loop->the_post();
              wc_get_template_part( 'content', 'product' );
            endwhile;
          } else {
            echo __( 'No products found' );
          }
          wp_reset_postdata();
        ?> 
              
              </ul> 
              </div>
    </div>
    </section>
    
    <section class="listas">
    <div class="container">
    <div class="row">
    <div class="col-md 6">
        <h2>Ei, da uma olhadinha nas categorias!</h2>
        <p>Organizamos tudinho por categorias e ocasiões pra você!
Tem muita coisa, algum desses é a sua cara. </p>
        </div>
    </div>
    <div class="row">
    <div class="col-md 4">
    <img src="#">
    <h3>Ocasiões</h3>    
    <a href="#">Aniversário</a>
    <a href="#">Casamento</a>
    <a href="#">Romântico</a>
    <a href="/loja/" class="link-mais">mais</a>
    </div> 
    <div class="col-md 4">
    <img src="#">
    <h3>Datas especiais</h3>    
    <a href="#">Dia dos Pais</a>
    <a href="#">Dia das Mães</a>
    <a href="#">Natal</a>
    <a href="/loja/" class="link-mais">mais</a>
    </div> 
    <div class="col-md 4">
    <img src="#">
    <h3>Clientes</h3>    
    <a href="#">Novo cliente</a>
    <a href="#">Há mais de 1 ano</a>
    <a href="#">Aniversário</a>
    <a href="/loja/" class="link-mais">mais</a>
    </div> 
    <div class="col-md 4">
    <img src="#">
    <h3>Corporativo</h3>    
    <a href="#">Funcionários</a>
    <a href="#">Contratados</a>
    <a href="#">Metas</a>
    <a href="/loja/" class="link-mais">mais</a>
    </div>
    <div class="col-md 4">
    <img src="#">
    <h3>Causas</h3>    
    <a href="#">Educação</a>
    <a href="#">Saúde</a>
    <a href="#">Animais</a>
    <a href="/loja/" class="link-mais">mais</a>
    </div> 
    <div class="col-md 4">
    <img src="#">
    <h3>Tipo do Produto</h3>    
    <a href="#">Livros</a>
    <a href="#">Revistas</a>
    <a href="#">Calendários</a>
    <a href="/loja/" class="link-mais">mais</a>
    </div>
    </div></div>
    </section>
  
    
    <section class="porque-comprar">
    <div class="container">
    <div class="row">
        <div class="col-md-7">
            <h3><?php the_field('comprar_subtitulo'); ?></h3>
            <h2 class="red"><?php the_field('comprar_titulo'); ?></h2>
            <p><?php the_field('comprar_texto'); ?></p>
            <a href="#" class="bt-red">Saiba mais</a>
        </div>
        <div class="col-md-5">
            
            <!-- Nav pills -->
  
  
  <ul class="nav nav-pills" role="tablist">
    <li class="nav-item">
      <a class="nav-link green active" data-toggle="pill" href="#tab-saude">Saúde</a>
    </li>
    <li class="nav-item">
      <a class="nav-link purple" data-toggle="pill" href="#tab-educacao">Educação</a>
    </li>
    <li class="nav-item">
      <a class="nav-link yellow" data-toggle="pill" href="#tab-animais">Animais</a>
    </li>
      <li class="nav-item">
      <a class="nav-link blue" data-toggle="pill" href="#tab-diversos">Diversos</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="tab-saude" class="container tab-pane active"><br>
      <h2 class="green"><?php the_field('saude_valor'); ?></h2>
      <p>Doados desde <strong><?php the_field('saude_ano'); ?></strong> para <strong><?php the_field('saude_projetos'); ?> projetos</strong> sociais graças à você!</p>
    </div>
    <div id="tab-educacao" class="container tab-pane fade"><br>
      <h2 class="purple"><?php the_field('educacao_valor'); ?></h2>
      <p>Doados desde <strong><?php the_field('educacao_ano'); ?></strong> para <strong><?php the_field('educacao_projetos'); ?> projetos</strong> sociais graças à você!</p>
    </div>
    <div id="tab-animais" class="container tab-pane fade"><br>
      <h2 class="yellow"><?php the_field('animais_valor'); ?></h2>
      <p>Doados desde <strong><?php the_field('animais_ano'); ?></strong> para <strong><?php the_field('animais_projetos'); ?> projetos</strong> sociais graças à você!</p>
    </div>
      <div id="tab-diversos" class="container tab-pane fade"><br>
     <h2 class="blue"><?php the_field('diversos_valor'); ?></h2>
      <p>Doados desde <strong><?php the_field('diversos_ano'); ?></strong> para <strong><?php the_field('diversos_projetos'); ?> projetos</strong> sociais graças à você!</p>
    </div>
  </div>
            <!-- -->
<a href="#" class="bt-white-red">Veja todos os 150 parceiros</a>

        </div>
        </div>
    </div>
</section>
    
<section class="blog-list">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
    
    <h2>Blog da<br> Banca do Bem</h2>
        </div>
        <?php
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 3
			);
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
		<div class="col-md-4 blogpost-home">
        <img src="<?php //the_post_thumbnail_url(); ?>">
            
            <h5><?php // the_date(); ?></h5>
            <h3><?php the_title(); ?></h3>
        </div>		
        
        
        <?php }
			endwhile;
		wp_reset_postdata();
	?> 
        </div></div>
</section>    
    <img class="woman-bottom" src="#">
 
</article><!-- #post-## -->

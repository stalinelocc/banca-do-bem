<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$causa = get_field('causa_produto');
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
<section class="top-produto">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <a href="/loja/" class="voltar">< Ver todos os Produtos</a>
            </div>
            <div class="col-md-6">
                <a href="/seja-um-embaixador/" class="link-embaixador"><img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_embaixador.png"> Seja um embaixador</a>
            </div>    
        </div>
        <div class="row">
            <div class="col-md-5">
            <h4 class="tipo">
                <?php echo get_the_term_list( $post->ID, 'tipo' ); ?>
                </h4>
            <?php   
                get_template_part( 'woocommerce/single-product/title' );
                get_template_part( 'woocommerce/single-product/short-description' );    
		?>
            <article class="dica-causa" style="background:url('<?php the_field('icone_voce_sabia', $causa)?>') top left no-repeat;">
                <h3>Você sabia?</h3>
                <p><?php the_field('voce_sabia_produto', $causa); ?></p>
                
            </article>
            <div class="botoes-desc-produto">
                <a href="<?php the_field('ver_paginas')?>" target="_blank" class="see-pages" >Veja algumas páginas</a>
                <a href="#" target="_blank" class=" btn-vermelho btn-padrao" >EU QUERO!</a>
            </div>
            </div>
            <div class="col-md-7">
                <?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action( 'woocommerce_before_single_product_summary' );
	?></div>
        </div>
        </div>
   
</section>

<section class="container mid-produto">
   <div class="bg-mid-produto"></div>
<div class="row">
    <div class="col-md-12">

	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">
		<?php
		
        
        /**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		do_action( 'woocommerce_single_product_summary' );
		?>
	</div>
    </div>

</div>
    </section>
    
    <?php
    $embalagem = get_field('produto_embalagem');
    $fotoembalagem = get_field('foto_embalagem');
    if ( $fotoembalagem ){
    ?>
<section class="embalagem">
    <div class="container">
    <div class="row">
        <div class="col-md-4">
             <a class="woosq-btn woosq-btn-22 " data-id="<?php echo $embalagem ?>" data-effect="mfp-3d-unfold">
                 <img src="<?php the_field('foto_embalagem') ?>"></a>
        </div>
        <div class="col-md-8">
        <?php the_field('texto_embalagem') ?>
            <a class="woosq-btn woosq-btn-22 " data-id="<?php echo $embalagem ?>" data-effect="mfp-3d-unfold"></a>
        </div>
        </div>
    </div>
</section>    
    <?php 
    }
    ?>

<section class="causa-resultado">
    <?php 
    
    $instituicao = get_field('instituicao');
    ?>
    <div class="container">
    <div class="row">
    <div class="col-12 text-center">
    <h2>Isso é mais do que um livro...</h2>
    <div class="imagem-texto">
        <div class="item-1">
        <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/agenda_vermelha.png"> 1 livro
        </div> 
        <div class="item-2"> = </div>
        <div class="item-3">
         <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/casa_vermelha.png"> 1 dia de aula
        </div>
    </div>
    <p>É bom pra todo mundo! Ao comprar este livro, você se inspira com um conteúdo incrível e colabora com uma causa que importa.</p>    
    </div>
    </div>
    </div>
</section>
<section class="fazer-diferenca">
    <div class="container">
<div class="row">
<div class="col-md-12">   
    <h2><?php 
        the_field('titulo_pagina', $causa); ?></h2>
</div>
    <div class="col-md-3 col-sm-6 col-6">
        <img src=" <?php the_field('diferenca_icone_1', $causa); ?>">
        <p><?php the_field('diferenca_texto_1', $causa); ?></p>
    </div>
    <div class="col-md-3 col-sm-6 col-6">
        <img src=" <?php the_field('diferenca_icone_2', $causa); ?>">
        <p><?php the_field('diferenca_texto_2', $causa); ?></p>
    </div>
    <div class="col-md-3 col-sm-6 col-6">
       <img src=" <?php the_field('diferenca_icone_3', $causa); ?>">
        <p><?php the_field('diferenca_texto_3', $causa); ?></p>
    </div>
    <div class="col-md-3 col-sm-6 col-6">
       <img src=" <?php the_field('diferenca_icone_4', $causa); ?>">
        <p><?php the_field('diferenca_texto_4', $causa); ?></p>
    </div>
  <div class="col-md-12">   
    <a href="/impacto/" class="bt-white">Saiba mais</a>
</div>  
        </div></div>
</section>
    
<section class="causa-numeros">
    
    <div class="container">
        <div class="row">
    <div class="col-md-12">
    <h2>O que isso já gerou de impacto em <?php echo get_the_title( $causa ); ?></h2>
    </div>
    <div class="col-md-4">
    <img src="<?php the_field('causa_icone_1', $causa); ?>">
    <h3><?php the_field('causa_numero_1', $causa); ?></h3>
    <p><?php the_field('causa_descritivo_1', $causa); ?></p>   
    </div>
    <div class="col-md-4">
    <img src="<?php the_field('causa_icone_2', $causa); ?>">
    <h3><?php the_field('causa_numero_2', $causa); ?></h3>
    <p><?php the_field('causa_descritivo_2', $causa); ?></p>   
    </div>
    <div class="col-md-4">
    <img src="<?php the_field('causa_icone_3', $causa); ?>">
    <h3><?php the_field('causa_numero_3', $causa); ?></h3>
    <p><?php the_field('causa_descritivo_3', $causa); ?></p>   
    </div>
    </div>
    </div>
</section>
 <section class="divisao">
    <div class="container">
        <div class="row">
    <div class="col-md-6"> 
    <h2>Veja o que acontece com os reais que você paga
por este produto…</h2>  
    </div>
            <div class="col-md-12"?>
            <img src="<?php the_field('divisao_grafico'); ?>">
            </div>
     </div></div>
</section>
<section class="instituicao">
    <div class="container">
    <div class="row">
    <div class="col-md-10">
        <h2>Quem você apoia com este produto?</h2>
        <h4><?php the_field('subtitulo', $instituicao); ?></h4>
        <p><?php the_field('instituicao_descricao', $instituicao); ?></p>
        <h5><a href="http://<?php the_field('site', $instituicao); ?>"><?php the_field('site', $instituicao); ?></a></h5>
        <img src="<?php the_field('logo', $instituicao); ?>">
    </div>
    </div></div>
</section>

<section class="detalhes-tecnicos">
    <div class="container">
<div class="row">
    <div class="col-md-12">
    <h2>Detalhes técnicos desse produto</h2></div>
    <div class="col-md-6">
    <?php the_field('detalhes_tecnicos_col_1'); ?>
    </div>
    <div class="col-md-6">
    <?php the_field('detalhes_tecnicos_col_2'); ?>
    </div>
        </div></div>
</section>

    </div>

<?php do_action( 'woocommerce_after_single_product' ); ?>

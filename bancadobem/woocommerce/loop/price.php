<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
?>

<?php if ( $price_html = $product->get_price_html() ) : ?>
	<span class="price"><?php echo $price_html; ?></span>
<?php endif; 

$causa = get_field('causa_produto');
if ( $causa == '249' ){
?>
<div class="icone-educacao"><img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico-loop-educacao.png"> apoie a educação</div>
<?php }
if ( $causa == '445' ){ ?>
<div class="icone-saude"><img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico-loop-saude.png"> apoie a saúde</div>
<?php }

if ( $causa == '446' ){ ?>
<div class="icone-animais"><img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico-loop-animais.png"> apoie os animais</div>
<?php }

if ( $causa == '792' ){ ?>
<div class="icone-diversos"><img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico-loop-diversos.png"> apoie diversos</div>
<?php } else{ ?>
<?php } 
?>

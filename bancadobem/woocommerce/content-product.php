<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( 'hvrbox', $product ); ?>>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
 <div class="hvrbox-layer_top">
<?php do_action( 'woocommerce_shop_loop_item_title' );?>
     <?php 
    $causa = get_field('causa_produto');
    ?>
<p class="descricao"><?php the_field('descritivo_loop') ?></p>
     <div class="apelo-causa">
     <div class="row">
		<div class="col-md-12">
			<div class="blc-imgs">
				<img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/agendavermelha.png" width="34" height="45"> 
				<img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/aimgigl.png" width="39" height="27">
				<img src="<?php the_field('icone_beneficio', $causa); ?>" width="39">
		</div>
	   <div class="blc-infos">
		   <span>1 livro</span>  <span>1 aula grátis</span>
		</div>

    	<p class="blc-txt">Ao comprar esse livro, você se inspira com um conteúdo incrível e colabora com uma causa que importa.</p>
    </div>     
         
     </div> 
     </div>
     <div class="row bottom-hover-loop">
         <div class="col-md-6" style="text-align:center;">
         <?php	do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
         </div>
         <div class="col-md-6"><a href="<?php the_permalink();?>">COMPRAR</a></div>
         <div class="col-md-12 alerta">
         Aproveite nossos preços especiais com <strong>descontos progressivos</strong> para grandes volumes.
         </div>
     </div>

            
            </div>
</li>

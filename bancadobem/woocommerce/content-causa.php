<?php
/**
 * Template part for displaying page content in single-causa.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <a href="/impacto/" class="voltar">< Voltar para Impacto</a>
        <header class="entry-header top-causa">
    <div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <h1><?php the_field('titulo'); ?></h1>
        <h2><?php the_field('subtitulo'); ?></h2>    
        </div>    
        <div class="col-md-3"></div>
    </div>
        
    </div>
    </header>
    <section class="causa_doados">
    <div class="container">
        <div class="row">
        <div class="col-md-4"><img src="#" width="100%" height="100"></div>
        <div class="col-md-8">
            <div class="col-md-12">
            <?php
                if (is_single('445')) { ?>
                     <h3><span>R$</span><?php the_field('saude_valor', 284 ); ?></h3>   
            <p>doados para <strong><?php the_field('saude_projetos', 284); ?> projetos</strong> sociais ligados à <?php the_title(); ?></p>
                
                <?php  } if (is_single('249')) { ?>
               <h3><span>R$</span><?php the_field('educacao_valor', 284 ); ?></h3>   
            <p>doados para <strong><?php the_field('educacao_projetos', 284); ?> projetos</strong> sociais ligados à <?php the_title(); ?></p>
                
                <?php  } if (is_single('446')) { ?>
                 <h3><span>R$</span><?php the_field('animais_valor', 284 ); ?></h3>   
            <p>doados para <strong><?php the_field('animais_projetos', 284); ?> projetos</strong> sociais ligados à <?php the_title(); ?></p>
                <?php  } if (is_single('792')) { ?>
                <h3><span>R$</span><?php the_field('diversos_valor', 284 ); ?></h3>   
            <p>doados para <strong><?php the_field('diversos_projetos', 284); ?> projetos</strong> sociais ligados à <?php the_title(); ?></p>
                <?php  } ?>
            </div>
            <div class="col-md-4">
            <h4><?php the_field('no_ongs'); ?> ONGs</h4>
                e causas apoiadas
            </div>
            <div class="col-md-4">
            <h4><?php the_field('no_edicoes'); ?> edições</h4>
                de revistas produzidas
            </div>
            <div class="col-md-4">
            <h4><?php the_field('no_produtos'); ?> produtos</h4>
                lançados e vendidos
            </div>
            
        </div>
        
        
        </div>
        
        </div>
    
    </section>
    
   
    <section class="causa-numeros">
    
    <div class="container">
        <div class="row">
    <div class="col-12">
    <h2>O que isso já gerou de impacto na causa da saúde?</h2>
    </div>
    <div class="col-4">
    <img src="<?php the_field('causa_icone_1'); ?>">
    <h3><?php the_field('causa_numero_1'); ?></h3>
    <p><?php the_field('causa_descritivo_1'); ?></p>   
    </div>
    <div class="col-4">
    <img src="<?php the_field('causa_icone_2'); ?>">
    <h3><?php the_field('causa_numero_2'); ?></h3>
    <p><?php the_field('causa_descritivo_2'); ?></p>   
    </div>
    <div class="col-4">
    <img src="<?php the_field('causa_icone_3'); ?>">
    <h3><?php the_field('causa_numero_3'); ?></h3>
    <p><?php the_field('causa_descritivo_3'); ?></p>   
    </div>
    </div>
    </div>
</section>
        
<section class="sua-compra">
    <div class="container">
    <div class="row">
        <div class="col-md-3">
            <img src="<?php the_field('causa_foto_1'); ?>" class="foto-causa">
            <img src="<?php the_field('causa_foto_2'); ?>" class="foto-causa">
            <img src="<?php the_field('causa_foto_3'); ?>" class="foto-causa">
        </div>
        <div class="col-md-6">
            <h2>O que a sua compra apoia</h2>
            <div class="row">
            <div class="col-md-4">
                <img src="<?php the_field('causa-foto-centro-1'); ?>">
            </div>
            <div class="col-md-8">
            <?php the_field('causa-texto-centro-1'); ?>
            </div>
            <div class="col-md-4">
                <img src="<?php the_field('causa-foto-centro-2'); ?>">
            </div>
            <div class="col-md-8">
            <?php the_field('causa-texto-centro-2'); ?>
            </div>
            <div class="col-md-4">
                <img src="<?php the_field('causa-foto-centro-3'); ?>">
            </div>
            <div class="col-md-8">
            <?php the_field('causa-texto-centro-3'); ?>
            </div>
                
            </div>
        </div>
        <div class="col-md-3">
            <img src="<?php the_field('causa_foto_4'); ?>" class="foto-causa">
            <img src="<?php the_field('causa_foto_5'); ?>" class="foto-causa">
            <img src="<?php the_field('causa_foto_6'); ?>" class="foto-causa">
        </div>

        </div>
    </div>
</section>    
<section class="causas-produtos">
    <div class="container">
    <div class="row">
        <div class="col-md-4"><a href="<?php the_field('link-produto-1'); ?>"><img src="<?php the_field('foto-produto-1'); ?>"></a></div>
        <div class="col-md-4"><a href="<?php the_field('link-produto-2'); ?>"><img src="<?php the_field('foto-produto-2'); ?>"></a></div>
        <div class="col-md-4"><a href="<?php the_field('link-produto-3'); ?>"><img src="<?php the_field('foto-produto-3'); ?>"></a></div>
        <div class="col-md-12">
        <?php
                if (is_single('445')) { ?>
                     <a href="/categoria-produto/causas/saude/" class="bt-red" >Veja todos os produtos</a>
                
                <?php  } if (is_single('249')) { ?>
               <a href="/categoria-produto/causas/educacao/" class="bt-red" >Veja todos os produtos</a>
                
                <?php  } if (is_single('446')) { ?>
                  <a href="/categoria-produto/causas/animais/" class="bt-red" >Veja todos os produtos</a>
                <?php  } if (is_single('792')) { ?>
                 <a href="/categoria-produto/causas/diversos/" class="bt-red" >Veja todos os produtos</a>
                <?php  } ?>
            
        </div>
    </div>
    </div>    
</section>	
   <section class="ongs_apoiadas">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
        <h2 class="red">Conheça as ONG's apoiadas</h2>
        </div>
        <div class="col-md-12">
            <ul class="row">
        <?php 

          $args = array(
            'order' => 'ASC',
            'posts_per_page' => '8',
            'post_type' => 'instituicao'
          );
          $loop = new WP_Query( $args );
          while ( $loop->have_posts() ) : $loop->the_post(); 
          
                $url = get_the_post_thumbnail_url(get_the_ID(), 'full');
        ?>
                
                
        <li class="marca-ong col-md-3">
        <a href="<?php the_field('site'); ?>" target="_blank"><img src="<?php the_field('logo'); ?>"></a>
        </li>
            <?php  endwhile;
          wp_reset_query();
// End of the loop.
          ?>
            </ul></div>
        </div>
    </div>
</section>

	
</article><!-- #post-## -->

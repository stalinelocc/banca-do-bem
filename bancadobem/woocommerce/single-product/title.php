<script>
function myFunction() {
  var x = document.getElementById("bloco-produto-parceiro");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>

<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce/Templates
 * @version    1.6.4
 */
$parceiro = get_field('parceiro');
$logoparceiro = get_field('logo', $parceiro );
$textparceiro = get_field('descricao_parceiro', $parceiro );
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

the_title( '<h1 class="product_title entry-title">', '</h1>' );
?>
<!--<a onclick="myFunction()"><img src="<?php //echo $logoparceiro ?>" style="position: absolute; right: 0; width: 50px; top: 0;"></a>
<div id="bloco-produto-parceiro" style="display:none;position: absolute;
background: #fff;
border: 3px solid #eee;
padding: 15px;
z-index: 10;
">
<img src="<?php //echo $logoparceiro ?>">
<?php //echo $textparceiro ?>
</div>-->
<?php

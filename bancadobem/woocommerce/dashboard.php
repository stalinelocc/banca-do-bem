<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<h2 class="obrigado">Muito Obrigado :)</h2> 
<?php
$current_user = wp_get_current_user();
?>
<h3 class="intro-account"><?php echo '' . $current_user->user_firstname . '&nbsp;';
    echo '' . $current_user->user_lastname . '';?>, você está gerando impacto no mundo com nossos produtos desde <?php echo date("d/m/Y", strtotime(get_userdata(get_current_user_id( ))->user_registered)); ?></h3>

<p><?php
	printf(
		__( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a>, and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' ),
		esc_url( wc_get_endpoint_url( 'orders' ) ),
		esc_url( wc_get_endpoint_url( 'edit-address' ) ),
		esc_url( wc_get_endpoint_url( 'edit-account' ) )
	);
?></p>
<section class="seu-apoio">
    <div class="row">
        <div class="col-md-4">
<h2>Sabe como o seu apoio impactou?</h2>
    <p>Veja como o seu apoio pode fazer a diferença.</p>
        </div>
        <div class="col-md-4"></div>
    <div class="col-md-4">
    <p><span class="total">
       <?php public function get_customer_total_order() {
    $customer_orders = get_posts( array(
        'numberposts' => - 1,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => array( 'shop_order' ),
        'post_status' => array( 'wc-completed' )
    ) );

    $total = 0;
    foreach ( $customer_orders as $customer_order ) {
        $order = wc_get_order( $customer_order );
        $total += $order->get_total();
    }

    return $total;
}
        
        
        R$29,90</span><br>
        foram doados até hoje <strong>por você.</strong>
        </p>
        <p>
        <span class="qtdade">3 ONGs</span><br>
        e 6 causas apoiadas</p>
        <p></p>
            
            <?php
            function get_user_orders_total($user_id) {
    // Use other args to filter more
    $args = array(
        'customer_id' => $user_id
    );
    // call WC API
    $orders = wc_get_orders($args);

    if (empty($orders) || !is_array($orders)) {
        return false;
    }

    // One implementation of how to sum up all the totals
    $total = array_reduce($orders, function ($carry, $order) {
        $carry += (float)$order->get_total();

        return $carry;
    }, 0.0);

    return $total;
}
        echo $total
            ?>
 
            
            
            <!--span class="qtdade">9 pessoas</span> <br>   
        visitaram nosso site graças à você
        </p-->
        
        </div>
    
    </div>

</section>
<section class="apoio-tipo-causa">
<h3>Quanto você apoiou cada tipo de causa</h3>

    <div class="row">
       
        <div class="col-12">
        <div class="row">
            
        <div class="col-md-3">
            <article class="hvrbox">
            <div class="cell">
                <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_saude.svg">
            <h3>Saúde</h3>
                <p class="porcento">80%</p>
            </div>
            
                </article>
            </div>
            
        <div class="col-md-3 hvrbox">
            <article class="hvrbox">
            <div class="cell">
                <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_educacao.svg">
            <h3>Educação</h3>
                <p class="porcento">80%</p>
            </div>
            </article></div>
            
        <div class="col-md-3 hvrbox">
            <article class="hvrbox">
            <div class="cell">
                <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_animais.svg">
            <h3>Animais</h3>
                <p class="porcento">80%</p>
            </div>
            </article></div>
            
        <div class="col-md-3 hvrbox">
            <article class="hvrbox">
            <div class="cell">
                 <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_diversos.svg">
            <h3>Diversos</h3>
                <p class="porcento">80%</p>
            </div>
            </article>
           </div>
            
        </div></div></div>

</section>
<section class="espalhe">
<h2>Espalhe agora</h2>
    <p>Com a sua ajuda, seremos a maior comunidade de produtos focados no bem do Brasil!</p>
<a class="social facebook" href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i>&nbsp;
 Facebook</a>
<a class="social twitter" href="#"><i class="fab fa-twitter" aria-hidden="true"></i>&nbsp; Twitter
</a>
<a class="social pinterest" href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i>&nbsp; Pin
</a>
<a class="social linkedin" href="#"><i class="fab fa-linkedin" aria-hidden="true"></i>&nbsp; Linkedin
</a>
<a class="social whatsapp" href="https://wa.me/?text=Conheça a www.bancadobem.com.br"><i class="fab fa-whatsapp" aria-hidden="true"></i> &nbsp;WhatsApp
</a>

</section>
<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */

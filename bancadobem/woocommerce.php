<?php
/**
 * The template for displaying Woocommerce Product
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

if ( is_page( 'carrinho' ) ) {
    ?> ERRO <?php
        get_header('checkout');
    // either in about us, or contact, or management page is in view
} else {
get_header();     
    // none of the page about us, contact or management is in view
}
 ?>
</div></div>

        <main id="main" class="site-main" role="main">

            <?php woocommerce_content(); ?>

        </main><!-- #main -->
   

<?php
//get_sidebar();
get_footer();

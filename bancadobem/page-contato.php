<?php
/**
 * Template Name: Contato
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
</div></div>
	
		<main id="main" class="site-main" role="main">
 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
     <section class="contato">
         <div class="container">
         <?php echo do_shortcode('[contact-form-7 id="788" title="Pag Contato"]'); ?>
             </div>
             </section>
            
            </article>
		</main><!-- #main -->
	

<?php
//get_sidebar();
get_footer();

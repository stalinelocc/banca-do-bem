<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */


get_header();     
 ?>

	<section id="primary" class="content-area col-sm-12 col-lg-8">
		<main id="main" class="site-main blog" role="main">
<h1>Blog Banca do Bem</h1>
<div class="principal">

           
			 <?php $blog1 = new WP_Query('posts_per_page=1'); ?>

<?php while ($blog1->have_posts()) : $blog1->the_post(); 

				get_template_part( 'template-parts/content', 'blog' );

                // If comments are open or we have at least one comment, load up the comment template.

			endwhile; // End of the loop.
            wp_reset_query();
			?>
</div> 
<div class="row blog-query-2">
   
    <?php 
    $blog2 = new WP_Query('posts_per_page=6&offset=1'); 
			  
while ($blog2->have_posts()) : $blog2->the_post(); ?>
    <div class="col-md-6">
        <?php get_template_part( 'template-parts/content', 'blog' ); ?>
    </div>
   <?php endwhile; // End of the loop.
            wp_reset_query();
			?>         
            
</div>            
		</main><!-- #main -->
	</section><!-- #primary -->
<section class="sidebar col-lg-4">
<?php
get_sidebar();
?>
</section>
<?php
get_footer();

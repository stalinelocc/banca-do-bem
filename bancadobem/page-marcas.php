<?php
/**
 * Template Name: Marcas
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
</div></div>
	
		<main id="main" class="site-main" role="main">
 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
     <header class="entry-header top-causa">
    <div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <h1>Marcas parceiras</h1>
        <h2>Criamos um novo tipo de produto – e também uma nova forma de trabalhar com empresas. Esqueça a ideia de cliente e fornecedor. Aqui, conversamos de igual para igual para criar projetos ganha-ganha, onde cada um faz o que sabe melhor, com autonomia, empatia, colaboração e transparência. Os resultados? Incríveis. Conheça as marcas de quem temos orgulho de ser parceiros.</h2>    
        </div>    
        <div class="col-md-3"></div>
    </div>
        
    </div>
    </header>
     <section class="marcas-parceiras">
         <div class="container">
         <div class="row">
			<?php 

      $args = array(
        'order' => 'ASC',
        'posts_per_page' => '100',
        'post_type' => 'marca'
      );
      $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post(); 

				get_template_part( 'template-parts/content', 'marcas' );


			endwhile; // End of the loop.
			?>
             </div></div>
             </section>
            
            </article>
		</main><!-- #main -->
	

<?php
//get_sidebar();
get_footer();

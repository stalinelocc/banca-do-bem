<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    
<?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans:300,400,500,600,800&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/inc/assets/css/customize.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/inc/assets/css/customize2.css">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script>
$(document).ready(function(){
    $('#show').click(function() {
      $('.searchbox').toggle("complete");
		$('i').toggle();
    });
	
});
$(document).ready(function(){
    $('#close').click(function() {
      $('.searchbox').toggle("slide");
    });
	
});
	
function menufull() {
  var x = document.getElementById("menufullscreen");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
        
function searchtop() {
  var x = document.getElementById("searchbox");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
        
</script>	

    
	</head>

<body <?php body_class(); ?>>
    <div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-starter' ); ?></a>
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
	
	
	<header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?>" role="banner">
      <div class="navbar-brand">
        <a href="http://rustrucks.com.br/">
            <img src="http://webapp136494.ip-172-104-13-105.cloudezapp.io/wp-content/uploads/2019/08/logo2x@2x.png" alt="Banca do Bem">
        </a>    
      </div>
        <div class="container">
			<a href="#" onclick="javascript.back();" class="red-burger" onclick="menufull()">
				<span><</span> Voltar para o site
				</a>
        </div>
	</header><!-- #masthead -->
    
	<div id="content" class="site-content">
		<div class="container">
			<div class="row">
                <?php endif; ?>
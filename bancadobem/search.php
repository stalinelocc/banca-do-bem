<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
</div></div></div>
	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<main id="main" class="site-main" role="main">
		<?php
		if ( have_posts() ) : ?>
<div class="container">
			<header class="page-header row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                <h1 class="page-title">Prontinho...veja o que encontramos</h1>
                <h3>Encontramos <?php /* Search Count */ $count = $wp_query->post_count; echo $count . ' '; wp_reset_query(); ?> resultados com a palavra <strong>"<?php printf( esc_html__( '%s', 'wp-bootstrap-starter' ), '<span>' . get_search_query() . '</span>' ); ?>"</strong>
                       
                    </h3></div>
                <div class="col-md-3"></div>
			</header><!-- .page-header -->
<section class="row">
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
</section>
            </div>      
		</main><!-- #main -->
	</section><!-- #primary -->

<section class="bottom-busca">        
    <h2>Ainda não encontrou?</h2>
            <div class="search-container">
                 <i class="fa fa-search" aria-hidden="true"></i> <?php get_search_form(); ?>
    </div>
<img src="/wp-content/themes/bancadobem/inc/assets/img/busca.jpg" width="60%" style="margin-top:15px;">
            <p>Se ainda precisar de ajuda, fale com a gente pela área de contato.</p>
            </section>

<?php
//get_sidebar();
get_footer();

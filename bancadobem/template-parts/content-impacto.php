<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header top-impacto" style="background:url('<?php the_field('bg_topo_impacto'); ?>) top center no-repeat;
background-size:cover;
    padding:220px 0 115px 0;">
    <div class="container">
    <div class="row">
        <div class="col-md-6">
        <h1><?php the_field('titulo'); ?></h1>
        <p><?php the_field('subtitulo'); ?></p>    
        </div>    
    </div>
    </div>
    </header>
    <section class="causas-apoio" style="background: url('<?php the_field('bg_apoio'); ?>') 32% 10% no-repeat;">
    <div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2 class="red"><?php the_field('causas_titulo'); ?></h2>
        </div>
        <div class="col-md-6">
            <h3><?php the_field('causas_introducao'); ?></h3>
        </div>
        <div class="col-12">
        <div class="row">
            
        <div class="col-md-3">
            <article class="hvrbox">
            <div class="cell">
                <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_saude.svg">
            <h3><?php the_field('causa1'); ?></h3>
            </div>
            <div class="hvrbox-layer_top">    
             <div class="cell">
                 <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_saude_color.svg">
            <p><?php the_field('causa1_texto'); ?></p>
            <a class="bt-white-red" href="/causa/saude/">Saiba mais</a>
                </div></div>
                </article>
            </div>
            
        <div class="col-md-3 hvrbox">
            <article class="hvrbox">
            <div class="cell">
                <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_educacao.svg">
            <h3><?php the_field('causa2'); ?></h3>
            </div>
            <div class="hvrbox-layer_top">    
             <div class="cell">
                 <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_educacao_color.svg">
            <p><?php the_field('causa2_texto'); ?></p>
            <a class="bt-white-red" href="/causa/educacao/">Saiba mais</a>
                </div></div></article></div>
            
        <div class="col-md-3 hvrbox">
            <article class="hvrbox">
            <div class="cell">
                <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_animais.svg">
            <h3><?php the_field('causa3'); ?></h3>
            </div>
            <div class="hvrbox-layer_top">    
             <div class="cell">
                 <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_animais_color.svg">
            <p><?php the_field('causa3_texto'); ?></p>
            <a class="bt-white-red" href="/causa/animais/">Saiba mais</a>
                </div></div></article></div>
            
        <div class="col-md-3 hvrbox">
            <article class="hvrbox">
            <div class="cell">
                 <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_diversos.svg">
            <h3><?php the_field('causa4'); ?></h3>
            </div>
             <div class="hvrbox-layer_top">    
            <div class="cell">
                <img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/ico_diversos_color.svg">
            <p><?php the_field('causa4_texto'); ?></p>
            <a class="bt-white-red" href="/causa/diversos/">Saiba mais</a>
                 </div></div></article>
           </div>
            
        </div></div></div></div>
    
    </section>
    
    <section class="causas-ciclo">
        <div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2 class="red"><?php the_field('ciclo_titulo'); ?></h2>
            <p><?php the_field('ciclo_introducao'); ?></p>
        </div>
        <div class="col-12">
            <img src="<?php the_field('ciclo_grafico'); ?>">
        </div>
            </div>
        </div>
    </section>    
    <section class="fazer-diferenca">
    <div class="container">
<div class="row">
<div class="col-12">   
    <h2><?php the_field('diferenca_titulo'); ?></h2>
    <p><?php the_field('diferenca_introducao'); ?></p>
</div>
    <div class="col-md-3">
        <img src=" <?php the_field('diferenca_icone_1'); ?>">
        <p><?php the_field('diferenca_texto_1'); ?></p>
    </div>
    <div class="col-md-3">
        <img src=" <?php the_field('diferenca_icone_2'); ?>">
        <p><?php the_field('diferenca_texto_2'); ?></p>
    </div>
    <div class="col-md-3">
       <img src=" <?php the_field('diferenca_icone_3'); ?>">
        <p><?php the_field('diferenca_texto_3'); ?></p>
    </div>
    <div class="col-md-3">
       <img src=" <?php the_field('diferenca_icone_4'); ?>">
        <p><?php the_field('diferenca_texto_4'); ?></p>
    </div> 
        </div></div>
</section>
    
   
<section class="porque-comprar">
    <div class="container">
    <div class="row">
        <div class="col-md-7">
            <h3><?php the_field('comprar_subtitulo'); ?></h3>
            <h2 class="red"><?php the_field('comprar_titulo'); ?></h2>
            <p><?php the_field('comprar_texto'); ?></p>
            <a href="/sobre" class="bt-red">Saiba mais</a>
        </div>
        <div class="col-md-5">
            
            <!-- Nav pills -->
  
  
  <ul class="nav nav-pills" role="tablist">
    <li class="nav-item">
      <a class="nav-link green active" data-toggle="pill" href="#tab-saude">Saúde</a>
    </li>
    <li class="nav-item">
      <a class="nav-link purple" data-toggle="pill" href="#tab-educacao">Educação</a>
    </li>
    <li class="nav-item">
      <a class="nav-link yellow" data-toggle="pill" href="#tab-animais">Animais</a>
    </li>
      <li class="nav-item">
      <a class="nav-link blue" data-toggle="pill" href="#tab-diversos">Diversos</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="tab-saude" class="container tab-pane active"><br>
      <h2 class="green"><?php the_field('saude_valor'); ?></h2>
      <p>Doados desde <strong><?php the_field('saude_ano'); ?></strong> para <strong><?php the_field('saude_projetos'); ?> projetos</strong> sociais graças à você!</p>
    </div>
    <div id="tab-educacao" class="container tab-pane fade"><br>
      <h2 class="purple"><?php the_field('educacao_valor'); ?></h2>
      <p>Doados desde <strong><?php the_field('educacao_ano'); ?></strong> para <strong><?php the_field('educacao_projetos'); ?> projetos</strong> sociais graças à você!</p>
    </div>
    <div id="tab-animais" class="container tab-pane fade"><br>
      <h2 class="yellow"><?php the_field('animais_valor'); ?></h2>
      <p>Doados desde <strong><?php the_field('animais_ano'); ?></strong> para <strong><?php the_field('animais_projetos'); ?> projetos</strong> sociais graças à você!</p>
    </div>
      <div id="tab-diversos" class="container tab-pane fade"><br>
     <h2 class="blue"><?php the_field('diversos_valor'); ?></h2>
      <p>Doados desde <strong><?php the_field('diversos_ano'); ?></strong> para <strong><?php the_field('diversos_projetos'); ?> projetos</strong> sociais graças à você!</p>
    </div>
  </div>
            <!-- -->
<a href="/marcas-parceiras/" class="bt-white-red">Veja todos os 150 parceiros</a>

        </div>
        </div>
    </div>
</section>
    
    
<section class="impacto-marcas">
    <div class="container">
    <div class="row">
        <div class="col-12">
            <h3>Veja algumas das marcas que já são parceiras</h3>
            <ul class="row">
                <?php 

      $args = array(
        'order' => 'ASC',
        'posts_per_page' => '8',
        'post_type' => 'marca'
      );
      $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post();           
      $thumbnail = get_the_post_thumbnail_url();
                
                ?>
            <li class="col-md-3">
               <img src="<?php echo $thumbnail ?>"> 
            </li>
                <?php endwhile; ?>
            </ul>
        </div>
        </div>
    </div>
</section>    
	
   

	
</article><!-- #post-## -->

<?php
/**
 * Template part for displaying page content in page-sobre.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header top-embaixador">
    <div class="container">
    <div class="row">
        <div class="col-md-4">
        <h1>Seja um embaixador
do bem</h1>
            <p>Ajude a espalhar e veja como o seu apoio na divulgação pode fazer a diferença.</p>
        </div>
        <div class="col-md-4">
        <img src="/wp-content/themes/bancadobem/inc/assets/img/embaixador.jpg">
        </div>
        <div class="col-md-4">
        <p>A cada</p>
            <h4>10 pessoas</h4>
        <p>que acessam nosso site</p>
        
        
        <h5>R$30,00</h5>
        <p>são doados <strong>graças à indicações feitas por pessoas como você</strong></p>    
        </div> 
        
    </div>
    </div>
    </header>
    <section class="espalhe">
<h2>Espalhe agora</h2>
    <p>Com a sua ajuda, seremos a maior comunidade de produtos focados no bem do Brasil!</p>
<a class="social facebook" href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i>&nbsp;
 Facebook</a>
<a class="social twitter" href="#"><i class="fab fa-twitter" aria-hidden="true"></i>&nbsp; Twitter
</a>
<a class="social pinterest" href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i>&nbsp; Pin
</a>
<a class="social linkedin" href="#"><i class="fab fa-linkedin" aria-hidden="true"></i>&nbsp; Linkedin
</a>
<a class="social whatsapp" href="https://wa.me/?text=Conheça a www.bancadobem.com.br"><i class="fab fa-whatsapp" aria-hidden="true"></i> &nbsp;WhatsApp
</a>

</section>
    
</article><!-- #post-## -->

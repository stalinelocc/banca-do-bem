<?php
/**
 * Template part for displaying page content in page-sobre.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header top-sobre">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
        <a href="#" class="selected">Sobre</a>
            <a href="/impacto">Nossas Causas</a>
            <a href="/marcas-parceiras/">Quem apóia</a>

        </div>
        <div class="col-md-6">
        <h1><?php the_field('titulo'); ?></h1>    
        </div> 
        <div class="col-md-6">
        <img src="<?php the_field('imagem_principal'); ?>">
        </div>
    </div>
    </div>
    </header>
    <section class="porque-comprar">
    <div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2 class="red"><?php the_field('comprar_titulo', 284); ?></h2>
            <p><?php the_field('comprar_texto', 284); ?></p>
            <a href="/impacto" class="bt-red">Veja os projetos apoiados</a>
        </div>
        <div class="col-md-6">
            
            <!-- Nav pills -->
  
  
  <ul class="nav nav-pills" role="tablist">
    <li class="nav-item">
      <a class="nav-link green active" data-toggle="pill" href="#tab-saude">Saúde</a>
    </li>
    <li class="nav-item">
      <a class="nav-link purple" data-toggle="pill" href="#tab-educacao">Educação</a>
    </li>
    <li class="nav-item">
      <a class="nav-link yellow" data-toggle="pill" href="#tab-animais">Animais</a>
    </li>
      <li class="nav-item">
      <a class="nav-link blue" data-toggle="pill" href="#tab-diversos">Diversos</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="tab-saude" class="container tab-pane active"><br>
      <h2 class="green"><?php the_field('saude_valor', 284); ?></h2>
      <p>Doados desde <strong><?php the_field('saude_ano', 284); ?></strong> para <strong><?php the_field('saude_projetos', 284); ?> projetos</strong> sociais graças à você!</p>
    </div>
    <div id="tab-educacao" class="container tab-pane fade"><br>
      <h2 class="purple"><?php the_field('educacao_valor', 284); ?></h2>
      <p>Doados desde <strong><?php the_field('educacao_ano', 284); ?></strong> para <strong><?php the_field('educacao_projetos', 284); ?> projetos</strong> sociais graças à você!</p>
    </div>
    <div id="tab-animais" class="container tab-pane fade"><br>
      <h2 class="yellow"><?php the_field('animais_valor', 284); ?></h2>
      <p>Doados desde <strong><?php the_field('animais_ano', 284); ?></strong> para <strong><?php the_field('animais_projetos', 284); ?> projetos</strong> sociais graças à você!</p>
    </div>
      <div id="tab-diversos" class="container tab-pane fade"><br>
     <h2 class="blue"><?php the_field('diversos_valor', 284); ?></h2>
      <p>Doados desde <strong><?php the_field('diversos_ano', 284); ?></strong> para <strong><?php the_field('diversos_projetos', 284); ?> projetos</strong> sociais graças à você!</p>
    </div>
  </div>
            <!-- -->


        </div>
        </div>
    </div>
</section>
<section class="conceito">
<div class="container">
    <div class="row">
    <div class="col-md-6">
        <?php the_field('texto_bloco_2'); ?>
        <a href="/impacto" class="bt-red">Veja seu impacto</a>
    </div>
    <div class="col-md-6">
        <img src="<?php the_field('imagem_bloco_2'); ?>">
    </div>
    </div>
</div>    
    
</section>    

<section class="banca">
<div class="container">
    <div class="row">
        <div class="col-md-4">
        <h2><?php the_field('titulo_bloco_3'); ?></h2>
        </div>
        <div class="col-md-8">
        <?php the_field('texto_bloco_3'); ?>       
        </div>
    </div>
</div>
</section>
 
<section class="muito-mais">
<div class="container">
    <div class="row">
    <div class="col-md-6">
      <img src="<?php the_field('imagem_bloco_4'); ?>">
    </div>
    <div class="col-md-6">
        <h3><?php the_field('titulo_bloco_4'); ?></h3>
       <?php the_field('texto_bloco_4'); ?>
    </div>
    </div>
</div>    
    
</section>        

 
<section class="ciclo-virtuoso">
<div class="container">
    <div class="row">    
        <div class="col-md-12">
             <h2><?php the_field('titulo_ciclo'); ?></h2></div>
        <div class="col-md-12"><h4><?php the_field('texto_ciclo'); ?></h4></div>
        <div class="col-md-12"><img src="<?php the_field('imagem_ciclo'); ?>"></div>
    </div>
</div>
</section>

<section class="marcas-causas">
<div class="container">
    <div class="row">    
        <div class="col-md-3"></div>
        <div class="col-md-3">
            <div class="box">
        <h3>Quais são as causas apoiadas?</h3>
        <a href="/impacto/" class="bt-red">Saiba mais ></a>
            </div></div>
        <div class="col-md-3">
            <div class="box">
        <h3>Marcas parceiras apoiadoras?</h3>
        <a href="/marcas-parceiras/" class="bt-red">Saiba mais ></a>
        </div></div>
        <div class="col-md-3"></div>
    </div>
</div>
</section>
<section class="filosofia">
<div class="container">
    <div class="row">    
    <div class="col-md-4">
        <img src="<?php the_field('imagem_1_-_bloco_5'); ?>" height="200"><br>
        <?php the_field('texto_1_bloco_5'); ?>
    </div>
    <div class="col-md-4">
        <img src="<?php the_field('imagem_2_-_bloco_5'); ?>" height="200"><br>
        <?php the_field('texto_2_bloco_5'); ?>
    </div>
    <div class="col-md-4">
        <img src="<?php the_field('imagem_3_-_bloco_5'); ?>" height="200"><br>
        <?php the_field('texto_3_bloco_5'); ?>
    </div>
    
    </div>
</div>
</section>
<section class="institucional">
<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
            <div class="col-md-4">
        <div class="row">
        <div class="col-md-6">
        <img src="/wp-content/themes/bancadobem/inc/assets/img/logo-mol.jpg">
        </div>
        <div class="col-md-6">
        <?php the_field('texto_mol_editora'); ?>
        </div>    
        </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4">
        <div class="row">
        <div class="col-md-6">
        <img src="/wp-content/themes/bancadobem/inc/assets/img/empresa-b.jpg">
        </div>
        <div class="col-md-6">
       <?php the_field('texto_empresa_b'); ?>
        </div>    
        </div>
        
        </div>
        <div class="col-md-2"></div>
    </div>
    </div>
</section>
	
</article><!-- #post-## -->

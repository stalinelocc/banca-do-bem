<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */
$thumbnail = get_the_post_thumbnail_url();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header top-sobre">
    <div class="row">
        <div class="col-md-6">
        <h1 class="entry-static"><?php the_field('titulo'); ?></h1>  
            <p><?php the_field('subtitulo'); ?></p>  
        </div> 
        <div class="col-md-6">
        <img src="<?php echo $thumbnail ?>">
        </div>
    </div>
    </header>

<?php the_content(); ?>

</article>


<!-- #post-## -->

<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<div class="col-md-4 search-result">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php //wp_bootstrap_starter_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
<div class="row">
    <div class="col-md-6">
    <?php
        if( get_post_type() == 'product' ) {
    ?>
        <h4>Produto</h4>
        
         <?php
        } else if( get_post_type() == 'post' ) {
    ?><h4>Blog</h4>
 
        <?php
        } else { ?>
   <h4>Página</h4>
<?php }
        ?>
        
        
        
        
        
    </div>
    <div class="col-md-6">
        <a class="bt-red" href="<?php the_permalink(); ?>">MAIS</a>
    
    </div></div>
	<footer class="entry-footer">
		<?php wp_bootstrap_starter_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
</div>
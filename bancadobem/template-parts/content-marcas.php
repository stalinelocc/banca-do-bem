<?php
/**
 * Template part for displaying page content in page-sobre.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */
$thumbnail = get_the_post_thumbnail_url();
?>

<div class="col-md-4">
    <div class="logo">
<img src="<?php echo $thumbnail ?>">
    </div>
<?php the_content(); ?>

</div>
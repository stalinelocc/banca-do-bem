<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
</div></div>
		<?php
		while ( have_posts() ) : the_post(); ?>
<section class="topbanner" style="background:url('<?php echo get_the_post_thumbnail_url($post_id, 'full'); ?>') top center; -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
padding:20px 0 300px 0;margin-bottom:30px;">
   <div class="container">
    <a href="/blog/">< VEJA MAIS POSTAGENS NO BLOG</a>
    </div>
</section>
<div class="container">
<div class="row">
<section id="primary" class="content-area col-sm-8 col-lg-8">
<main id="main" class="site-main" role="main">


			<?php get_template_part( 'template-parts/content', get_post_format() );

			  

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</section><!-- #primary -->
<section class="side-post col-md-4">
    <ul class="author">
         <li><?php echo get_avatar( get_the_author_meta( 'ID' ), 128 ); ?></li>
		<li><strong><?php the_author_meta('first_name'); ?> <?php the_author_meta('last_name'); ?></strong> </li>
         <li>
             <?php 
             $value = get_cimyFieldValue(get_the_author_ID(), 'DESCREVE');

if ($value != NULL) {
echo $value;
}
             
             
             ?>
             
             <?php the_author_meta('cimy_uef_1'); ?> </li>
         </ul>
    <a href="#" class="bt-red">Seja um Embaixador</a>
    <?php
    get_sidebar();?>
    </section>
<?php
get_footer();

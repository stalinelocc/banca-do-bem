<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    
<?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans:300,400,500,600,800&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/inc/assets/css/customize.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/inc/assets/css/custom-style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/inc/assets/css/custom-style2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
	<script>
$(document).ready(function(){
    $('#show').click(function() {
      $('.searchbox').toggle("complete");
		$('i').toggle();
    });
	
});
$(document).ready(function(){
    $('#close').click(function() {
      $('.searchbox').toggle("slide");
    });
	
});
        
        
	
function menufull() {
  var x = document.getElementById("menufullscreen");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
        
function searchtop() {
  var x = document.getElementById("searchbox");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
        
function modaltop() {
  var x = document.getElementById("modal--comprar");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}        
        
        
</script>	
<script type="text/javascript">

    jQuery(document).ready(function() {
        //jQuery('#menu-item-1993').find('a').attr('onclick', 'modaltop()');
        
    });
</script>
    
    <!-- OWL CAROUSEL -->
    <!--link rel="stylesheet" href="<?php //echo get_template_directory_uri(); ?>/inc/assets/owl/docs.theme.min.css"-->
	
	 <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/inc/assets/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/inc/assets/owl/owl.theme.default.min.css">
	
    <script src="<?php echo get_template_directory_uri(); ?>/inc/assets/owl/owl.carousel.js"></script>
    
	</head>

<body <?php body_class(); ?>>
<div id="menufullscreen" style="display:none">
    <a href="#" id="close" onclick="menufull()"><i class="fa fa-times fa-3x" aria-hidden="true"></i></a>
    <div class="container">
        <div class="row">
        <div class="col-md-6">
            <?php
                wp_nav_menu(array(
                'theme_location'    => 'big-full',
                'container'       => 'div',
                'container_id'    => 'fullscreen-nav',
                'menu_id'         => false,
                'menu_class'      => 'navbar-nav',
                'depth'           => 1,
                'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                'walker'          => new wp_bootstrap_navwalker()
                ));
                ?>
        </div>
        <div class="col-md-6" style="display: flex; flex-direction: column;">
            <?php
                wp_nav_menu(array(
                'theme_location'    => 'small-full',
                'container'       => 'div',
                'container_id'    => 'small-nav',
                'menu_id'         => false,
                'menu_class'      => 'navbar-nav',
                'depth'           => 1,
                'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                'walker'          => new wp_bootstrap_navwalker()
                ));
                ?>
                <a href="#" class="btn-padrao tamanho-botao carrinho-icon"> Meu Carrinho</a>
        </div>
        </div>
    </div>
    
    
</div>
    
    
    <div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-starter' ); ?></a>
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
	
	<div class="header-wrap">
		<header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?>" role="banner">
	        <div id="searchbox" style="display:none">
			
				 <a href="#" id="close" onclick="searchtop()"><i class="fa fa-times fa-3x" aria-hidden="true"></i></a>
				<div class="campo">
					<i class="fa fa-search fa-2x" aria-hidden="true"></i> <?php get_search_form(); ?>
				</div>
			
			</div>
	        <div class="modal-comprar" id="modal--comprar" style="display:none">
	            <div class="container">
	                <a href="#" id="close"><i class="fa fa-times fa-3x" aria-hidden="true"></i></a>
	                <div class="row">
	                
	                    <div class="col-md-3">
	                    <h4>Tipo</h4>
	                        <a href="/tipo/livros/">Livros</a><br>
	                        <a href="/tipo/revistas/">Revistas</a><br>
	                        <a href="/tipo/calendarios/">Calendários</a><br>
	                        <a href="/loja/" class="link--mais">mais</a>
	                    </div>
	                    <div class="col-md-3">
	                    <h4>Ocasiões</h4>
	                        <a href="/categoria-produto/ocasioes/aniversario/">Aniversário</a><br>
	                            <a href="/categoria-produto/ocasioes/batizado/">Batizado</a><br>
	                            <a href="/categoria-produto/ocasioes/casamento/">Casamento</a><br>
	                        <a href="/loja/" class="link--mais">mais</a>
	                    </div>
	                    <div class="col-md-3">
	                    <h4>Datas especiais</h4>
	                        <a href="/categoria-produto/datas-especiais/dia-dos-pais/">Dia dos pais</a><br>
	                            <a href="/categoria-produto/datas-especiais/dia-das-maes/">Dia das mães</a><br>
	                        <a href="/categoria-produto/datas-especiais/natal/">Natal</a><br>
	                        <a href="/loja/" class="link--mais">mais</a>
	                    </div>
	                    <div class="col-md-3">
	                    <h4>Causas</h4>
	                        <a href="/categoria-produto/causas/saude/"> Saúde</a><br>
	                        <a href="/categoria-produto/causas/animais/">Animais</a><br>
	                            <a href="/categoria-produto/causas/educacao/">Educação</a><br>
	                            <a href="/categoria-produto/causas/diversos/">Diversos</a><br>
	                        <a href="/loja/" class="link--mais">mais</a>
	                    </div>
	                </div>
	                
	            </div>
	        </div>
	        <div class="container">
				<a href="#" class="red-burger" onclick="menufull()">
					<i class="fa fa-bars"></i>
					</a>
	            <nav class="navbar navbar-expand-xl p-0">
	                <div class="navbar-brand">
	                    <?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
	                        <a href="<?php echo esc_url( home_url( '/' )); ?>">
	                            <img src="<?php echo esc_url(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
	                        </a>
	                    <?php else : ?>
	                        <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
	                    <?php endif; ?>

	                </div>
	                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
	                    <span class="navbar-toggler-icon"></span>
	                </button>

	                <?php
	                wp_nav_menu(array(
	                'theme_location'    => 'primary',
	                'container'       => 'div',
	                'container_id'    => 'main-nav',
	                'container_class' => 'collapse navbar-collapse justify-content-end',
	                'menu_id'         => false,
	                'menu_class'      => 'navbar-nav',
	                'depth'           => 3,
	                'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
	                'walker'          => new wp_bootstrap_navwalker()
	                ));
	                
	                ?>
	                
	    
					
				</nav>
					

	        </div>
	    </header><!-- #masthead -->
    </div>
    <div class="lateral-icons">
        <ul>
            <li><a href="#" onclick="searchtop()" class="searchtopo"><i class="fas fa-search"></i> </a></li>
            <?php echo do_shortcode("[shortcodeforcart]")?>
        </ul>
    </div>               

    <?php if(is_front_page() && !get_theme_mod( 'header_banner_visibility' )): ?>
        <div id="page-sub-header" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
            <div class="container">
                <h1>
                    <?php
                    if(get_theme_mod( 'header_banner_title_setting' )){
                        echo get_theme_mod( 'header_banner_title_setting' );
                    }else{
                        echo 'WordPress + Bootstrap';
                    }
                    ?>
                </h1>
                <p>
                    <?php
                    if(get_theme_mod( 'header_banner_tagline_setting' )){
                        echo get_theme_mod( 'header_banner_tagline_setting' );
                }else{
                        echo esc_html__('To customize the contents of this header banner and other elements of your site, go to Dashboard > Appearance > Customize','wp-bootstrap-starter');
                    }
                    ?>
                </p>
                <a href="#content" class="page-scroller"><i class="fa fa-fw fa-angle-down"></i></a>
            </div>
        </div>
    <?php endif; ?>
	<div id="content" class="site-content <?php echo is_page() && is_front_page() ? "" : "page-content-banca"; ?>">
		<div class="container">
			<div class="row">
                <?php endif; ?>